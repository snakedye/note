use snui::lens::*;
use snui::widgets::*;
use snui::*;
use snui_wayland::*;

#[derive(Debug, Clone, PartialEq)]
struct Note {
    color: u32,
    title: String,
    content: String,
}

#[derive(Debug, Clone, Copy, PartialEq)]
enum View {
    Notes,
    Trash,
}

#[derive(Debug, Clone, PartialEq)]
struct NoteJot {
    view: View,
    input: String,
    current: usize,
    notes: Vec<Note>,
}

impl Default for NoteJot {
    fn default() -> Self {
        NoteJot {
            input: String::new(),
            view: View::Notes,
            current: 2,
            notes: vec![
                Note {
                    color: color::RED,
                    title: "snui".to_string(),
                    content: "never lies".to_owned(),
                },
                Note {
                    color: color::BEIGE,
                    title: "Σ π".to_string(),
                    content: "perfection".to_string(),
                },
                Note {
                    color: color::BLUE,
                    title: "τ".to_string(),
                    content: "wrong".to_string(),
                },
                Note {
                    color: color::ORANGE,
                    title: "irrelevant".to_string(),
                    content: "I just needed another one.".to_string(),
                },
                Note {
                    color: color::YELLOW,
                    title: "* more".to_string(),
                    content: "I just needed another one.".to_string(),
                },
            ],
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
enum NoteListen {
    Title,
    Content,
}

impl Lens<String> for NoteJot {
    type Message = NoteListen;

    fn with<V, F: FnOnce(&String) -> V>(&self, message: &Self::Message, f: F) -> V {
        if self.notes.len() > self.current {
            match message {
                NoteListen::Title => f(&self.notes[self.current].title),
                NoteListen::Content => f(&self.notes[self.current].content),
            }
        } else {
            f(&String::new())
        }
    }
    fn with_mut<V, F: FnOnce(&mut String) -> V>(&mut self, _message: &Self::Message, f: F) -> V {
        let v = f(&mut self.input);
        self.current = (0..self.notes.len())
            .find(|i| self.notes[*i].title.contains(self.input.as_str()))
            .unwrap_or(self.current);
        v
    }
}

impl Lens<bool> for NoteJot {
    type Message = String;

    fn with<V, F: FnOnce(&bool) -> V>(&self, message: &Self::Message, f: F) -> V {
        f(&self
            .notes
            .get(self.current)
            .map(|note| note.title.eq(message.as_str()))
            .unwrap_or_default())
    }
    fn with_mut<V, F: FnOnce(&mut bool) -> V>(&mut self, message: &Self::Message, f: F) -> V {
        f(&mut self
            .notes
            .get(self.current)
            .map(|note| note.title.eq(message.as_str()))
            .unwrap_or_default())
    }
}

fn views() -> impl Widget<NoteJot> {
    [View::Notes, View::Trash]
        .into_iter()
        .map(|view| {
            Label::from(format!("{:?}", view))
                .class("")
                .with_fixed_size(100., 30.)
                .background(color::TRANSPARENT)
                .radius(3.)
                .button(
                    move |this, ctx: &mut UpdateContext<NoteJot>, _, p| match p {
                        Pointer::Enter => this.set_texture(color::BG2),
                        Pointer::Leave => this.set_texture(color::TRANSPARENT),
                        _ => {
                            if p.left_button_click().is_some() {
                                map!(ctx.as_mut(), view).replace(view);
                            }
                        }
                    },
                )
        })
        .collect::<Column<_, _>>()
}

fn views_menu() -> impl Widget<NoteJot> {
    Column::new()
        .with_child(Label::from("Views"))
        .with_spacing(10.)
        .with_child(views())
}

fn notes(notes: Vec<Note>) -> impl Widget<NoteJot> {
    notes
        .into_iter()
        .map(move |note| {
            Label::from(&note.title)
                .color(note.color)
                .class("title")
                .with_fixed_height(30.)
                .background(color::TRANSPARENT)
                .padding(5.)
                .radius(3.)
                .activate(note.title.clone(), move |this, active, _, _| match active {
                    true => this.set_texture(color::BG0),
                    false => this.set_texture(color::TRANSPARENT),
                })
                .button(
                    move |this, ctx: &mut UpdateContext<NoteJot>, _, p| match p {
                        Pointer::Enter => {
                            if !this.active() {
                                this.set_texture(color::BG3)
                            }
                        }
                        Pointer::Leave => {
                            if !this.active() {
                                this.set_texture(color::TRANSPARENT)
                            }
                        }
                        _ => {
                            if p.left_button_click().is_some() {
                                let current = (0..ctx.notes.len())
                                    .find(|i| ctx.notes[*i].title.eq(&note.title))
                                    .unwrap_or(ctx.current);
                                map!(ctx.as_mut(), current).replace(current);
                            }
                        }
                    },
                )
        })
        .collect::<Column<_, _>>()
}

fn main_view() -> impl Widget<NoteJot> {
    Column::new()
        .with_child(
            DynLabel::from("No selected note.")
                .map(NoteListen::Title)
                .class("title")
                .with_max_height(25.)
                .anchor(START, CENTER)
                .padding(5.)
                .border(color::BG2, 1.)
                .radius(5.),
        )
        .with_spacing(5.)
        .with_child(
            DynLabel::from("Please select a note.")
                .map(NoteListen::Content)
                .class("")
                .clamp()
                .anchor(START, START)
                .padding(10.),
        )
        .border(color::BG2, 1.)
        .top_right_radius(5.)
        .bottom_right_radius(5.)
        .padding(10.)
}

fn ui(list: Vec<Note>) -> impl Widget<NoteJot> {
    Row::new()
        .with_child(views_menu())
        .with_spacing(10.)
        .with_child(Column!(
            Input::from("")
                .map(NoteListen::Title)
                .class("")
                .with_fixed_height(30.)
                .anchor(START, START),
            Row!(
                Scrollbox::new(notes(list))
                    .background(color::BG2)
                    .padding(3.)
                    .top_left_radius(5.)
                    .bottom_left_radius(5.)
                    .with_fixed_width(200.)
                    .anchor(CENTER, START),
                main_view()
            )
        ))
}

fn main() {
    let notejot = NoteJot::default();

    let window = default_window(
        Label::from("Notes").class("title"),
        ui(notejot.notes.clone()).padding(10.),
    );

    WaylandClient::init(notejot, |client, qh| {
        client.set_app_id("notes");
        let mut view = client.create_window(window, qh);
        view.set_title("Notes".to_owned());
    })
}
